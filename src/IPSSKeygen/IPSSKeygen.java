package IPSSKeygen;

import IPSSKeygen.apu.util.StringUtil;
import IPSSKeygen.apu.util.Utility;
import IPSSKeygen.apu.util.Verifier;

import java.io.Console;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 1/28/13
 * Time: 2:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class IPSSKeygen {

    public static void main(String[] args) throws Exception {
        Utility.initOS();

        try {
            Verifier copyProtection = new Verifier();
            String licenseFile = Utility.appendDir(Utility.getCurrentDir(),"ip360.lic");
            if (Utility.fileExists(licenseFile) && !Utility.deleteFile(licenseFile,10))
                throw new Exception("e0x0100");
            copyProtection.saveLicense(licenseFile);
            //System.out.println("    File Hash = " + copyProtection.getFileHash());
            //System.out.println("File Mod Date = " + copyProtection.getFileModDate());
            //System.out.println("    File Size = " + copyProtection.getFileSize());
            //System.out.println("  Hardware ID = " + copyProtection.getHardwareId());
            //System.out.println("   Network ID = " + copyProtection.getNetworkId());
            //copyProtection.verifyLicense(licenseFile);
            System.out.println("Success!");
        } catch (Exception e) {
            System.out.println("Failed to Enable Copy Protection ("+ e.getMessage()+")");
            //e.printStackTrace();
        }


    }
}
